# Frontend Aufgabe "Valeriana"

- Vorlagen in `/screens`
- Source in `/src`
- Node Version: `14`
- Dev Server `yarn dev`
- Sämtliche Bilder vorbereitet in `/src/img`
- Bootstrap SCSS unter `/src/scss`
- Neue SCSS Files in `main.scss` einbinden
- Variablen für Bootstrap unter `variables.scss` ändern
- Weitere Abhängigkeiten mittels `yarn` hinzufügen

## Funktionen Startseite
- `/screens/01_0_Startseite.png`
- `/screens/01_1_Startseite.png` 
- `/screens/02_0_Startseite_mobil.png`
    - Bild / Text Slider (externe Libraries verwenden erwünscht!)
    - je nach Tageszeit des Slides ein unterschiedliches Theme
    - Responsive Darstellung
    
- `03_01_Menu.png`
- `04_02_Menu.png`
    - Fullscreen Menu (Mobile & Desktop)

## Blogartikel
- Blogartikel aus `blog.js` holen
- Für Slider wichtig

## Kontaktformular
- Vorname, Nachname, E-Mail required
- E-Mail Validierung einbauen
