const articles = [
  {
    id: 'fruehstuecksideen',
    title: '7.30 Lass es dir schmecken',
    intro: 'Guten Morgen! Wie startest du am besten in einen Tag ohne Stress? Mit einem ausgewogenen Frühstück natürlich!',
    time: {hour: 7, minute: 30, second: 0}
  },
  {
    id: 'uebungen',
    title: '15.30 Kurzurlaub für die Seele',
    intro: 'Auch die beste Planung kann Stress und Nervosität nicht immer verhindern. Wenn es doch einmal passiert, stell dich dem Stress nicht. Viel besser, du weichst aus.',
    time: {hour: 15, minute: 30, second: 0}
  },
  {
    id: 'sanft-hinuebergleiten',
    title: '21.30 Entspann dich',
    intro: 'Es wird Zeit, zur Ruhe zu kommen und sich ohne Stress auf die Nacht einzustimmen. Jetzt ist Entspannung pur angesagt: Wenn du Lust hast, lass dir zum Beispiel ein schönes warmes Bad ein.',
    time: {hour: 21, minute: 30, second: 0}
  }
]

export default articles
