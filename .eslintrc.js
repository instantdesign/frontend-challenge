module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true
  },
  extends: [
    "eslint:recommended",
    "plugin:vue/strongly-recommended"
  ],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly"
  },
  parserOptions: {
    // ecmaVersion: 10,
    sourceType: "module",
    impliedStrict: true
  },
  plugins: [
    "vue"
  ],
  rules: { }
}
