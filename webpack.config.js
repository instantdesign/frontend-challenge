const webpack = require('webpack')
const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const WebpackNotifierPlugin = require('webpack-notifier')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg')
const imageminPngquant = require('imagemin-pngquant')
const TerserJSPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = (env, argv) => {
  var production = argv.mode === 'production'

  var config = {
    output: {
      publicPath: '/'
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader'
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: file => (
            /node_modules/.test(file) &&
            !/\.vue\.js/.test(file)
          )
        },
        {
          test: /\.html$/,
          loader: 'html-loader',
          options: { minimize: true }
        },
        {
          test: /\.(scss|css)$/,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { sourceMap: !production } },
            { loader: 'postcss-loader', options: { sourceMap: !production } },
            { loader: 'sass-loader', options: { sourceMap: !production } }
          ]
        },
        {
          test: /\.(svg|png|jp(e*)g)$/,
          loader: 'url-loader',
          options: {
            limit: 4000, // Convert images < 4kb to base64 strings
            outputPath: 'img/',
            esModule: false
          }
        },
        {
          test: /\.(pdf)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'docs/[name].[ext]'
              }
            }
          ]
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.vue', '.json', '.scss'],
      alias: {
        'vue': 'vue/dist/vue.runtime.esm.js',
        'bootstrap-vue': 'bootstrap-vue/src/index.js',
        '@': path.join(__dirname, 'src')
      }
    },
    optimization: {
      minimizer: [
        new TerserJSPlugin({}),
        new OptimizeCSSAssetsPlugin({})
      ]
    },
    plugins: [
      new HtmlWebPackPlugin({
        template: './src/index.html',
        filename: './index.html'
      }),
      new MiniCssExtractPlugin(),
      new CleanWebpackPlugin(),
      new VueLoaderPlugin(),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      }),
      new CopyWebpackPlugin([
        { from: 'src/img/*' },
      ]),
      new ImageminPlugin({
        disable: !production,
        test: /\.(jpe?g|png|gif)$/i,
        jpegtran: null,
        plugins: [
          imageminMozjpeg({
            quality: 80,
            progressive: true
          }),
          imageminPngquant({
            speed: 1,
            quality: [0.6, 0.9]
          })
        ]
      }),
      new WebpackNotifierPlugin()
    ],
    devtool: production ? undefined : 'source-map',
    devServer: {
      open: true,
      compress: true,
      overlay: true,
      host: '0.0.0.0'
    }
  }

  if (argv.analyze) {
    config.plugins.push(new BundleAnalyzerPlugin())
  }

  return config
}
